package main

import (
  "log"
  "flag"
  "net/http"
)

var (
  flag_port = flag.String("port", ":8000", "Port to host the server on")
)

func main() {
  flag.Parse()

  http.Handle("/", http.FileServer(http.Dir("./public")))

  log.Println("Starting server on port", *flag_port)
  if err := http.ListenAndServe(*flag_port, nil); err != nil {
    log.Fatal(err)
  }
}
