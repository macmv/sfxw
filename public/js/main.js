var ctx = new (window.AudioContext || window.webkitAudioContext)()
var sound_type = "sin"

// This updates all of the settings of the sound. This is called whenever any buttons are pressed.
function update() {
  sound_type = $("input[type=radio]:checked")[0].id
}

// This is what buttons should call when they want to play audio.
function play() {
  // Makes sure settings are up to date
  update()
  status_line.innerText = "Generating..."

  generate_file(function(buf) {
    source = ctx.createBufferSource()
    source.buffer = buf
    source.connect(ctx.destination)
    source.start()
    status_line.innerText = "Playing"

    attack = setting_attack.value / 100
    sustain = setting_sustain.value / 100
    decay = setting_decay.value / 100
    length = attack + sustain + decay
    setTimeout(function() {
      status_line.innerText = "\n"
      source.stop()
    }, length * 1000)
  })
}

// This saves the sound as a .wav file
function download() {
  update()
  status_line.innerText = "Generating..."

  generate_file(function(buf) {
    save_file("sound.wav", generate_wav(buf, ctx.sampleRate))
    status_line.innerText = "\n"
  })
}

// This returns the sound at a given time.
// Sum is the sum of all frequencies (used to keep the wave stable in freq changes).
// Sr is the sample rate.
function sample(t, sum, sr) {
  // This is a value between 440 and 2440 hz
  freq = 440 + setting_freq.value * 20

  // This adds vibrato
  freq += Math.sin(t * Math.PI * (setting_vibrato_speed.value / 10)) * setting_vibrato_depth.value
  sum += freq

  // val = 0
  // All waves are based off of this, so we want to calculate it every time.
  val = Math.sin(2 * Math.PI * (sum/sr))
  if (sound_type == "saw") {
    // This converts it to a sawtooth
    val = Math.asin(val)
    // Sawtooth is loud, but not as loud as square waves
    val *= 0.5
  } else if (sound_type == "square") {
    // We make a sawtooth wave here
    val = Math.asin(val)
    // val is between 0 and 1, and we want to make it either 0 or 1, based on the duty cycle.
    // If duty cycle is 0, we want the wave to be on half of the time.
    // If duty cycle is 1, we want the waveee to always be on.
    if (val > setting_square_duty.value / 200 + 0.5) {
      // Square waves are really loud, so we make it go between 0.2 and -0.2
      val = 0.2
    } else {
      val = -0.2
    }
  } else if (sound_type == "noise") {
    val = Math.random()
    // Again, noise is loud, but not super loud
    val *= 0.5
  }
  volume = setting_volume.value / 100
  attack = setting_attack.value / 100
  sustain = setting_sustain.value / 100
  decay = setting_decay.value / 100
  if (t < attack) {
    volume += (t - attack) / attack
  } else if (t > attack + sustain && t < attack + sustain + decay) {
    volume -= (t - attack - sustain) / decay
  } else if (t > attack + sustain + decay) {
    // The audio clip is finished here
    return null
  }
  if (volume < 0) {
    volume = 0
  }
  val *= volume
  return [val, sum]
}

// This takes a callback for when the file is finished.
function generate_file(callback) {
  // 3 second clip
  buf = ctx.createBuffer(2, ctx.sampleRate * 3, ctx.sampleRate)
  left = buf.getChannelData(0)
  right = buf.getChannelData(1)

  // Generate 8k samples at a time
  section_length = 1024 * 8

  t = 0
  delta = 1 / ctx.sampleRate
  sum = 0
  function generate_section(index) {
    for (i = index; i < index + section_length; i++) {
      if (i > buf.length) {
        break
      }
      values = sample(t, sum, ctx.sampleRate)
      // This stops generating if the audio has ended
      if (values == null) {
        callback(buf)
        return
      }
      // We divide by two to make the loud sounds not hurt your ears
      val = values[0] / 2
      sum = values[1]
      left[i] = val
      right[i] = val
      t += delta
    }
    if (index + section_length > buf.length) {
      callback(buf)
      return
    }
    setTimeout(function() {
      generate_section(index + section_length)
    }, 1)
  }

  generate_section(0)
}

function save_file(filename, data) {
  decoder = new TextDecoder('utf8')

  element = document.createElement('a')
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + decoder.decode(data))
  element.setAttribute('download', filename)

  element.style.display = 'none'
  document.body.appendChild(element)

  element.click()

  document.body.removeChild(element)
}

function generate_wav(buf, sr) {
  // *2 is because 2 channels, *4 is because floats, and +44 is because of wav file header.
  len = buf.length * 2 * 4 + 44
  out = new ArrayBuffer(len)
  v = new DataView(out)

  bytes_per_sample = 4

  // Based on format from http://soundfile.sapp.org/doc/WaveFormat/

  // ----------------------- chunk descritor -----------------------
  v.setUint8(0, 'R'.charCodeAt(0))
  v.setUint8(1, 'I'.charCodeAt(0))
  v.setUint8(2, 'F'.charCodeAt(0))
  v.setUint8(3, 'F'.charCodeAt(0))
  v.setInt32(4, len - 8, true)
  v.setUint8(8, 'W'.charCodeAt(0))
  v.setUint8(9, 'A'.charCodeAt(0))
  v.setUint8(10, 'V'.charCodeAt(0))
  v.setUint8(11, 'E'.charCodeAt(0))

  // ----------------------- fmt chunk -----------------------

  v.setUint8(12, 'f'.charCodeAt(0))
  v.setUint8(13, 'm'.charCodeAt(0))
  v.setUint8(14, 't'.charCodeAt(0))
  v.setUint8(15, ' '.charCodeAt(0))

  v.setInt32(16, 16)                        // pcm data
  v.setInt16(20, 1)                         // no compression
  v.setInt16(22, 2)                         // stereo audio
  v.setInt32(24, sr)                        // sample rate
  v.setInt32(28, 2 * sr * bytes_per_sample) // channels * sample rate * bytes per sample (byte rate)
  v.setInt16(32, 2 * bytes_per_sample)      // channels * bytes per sample (total bytes per sample)
  v.setInt16(34, bytes_per_sample * 8)       // bits per sample

  // ----------------------- data chunk -----------------------

  v.setUint8(36, 'd'.charCodeAt(0))
  v.setUint8(37, 'a'.charCodeAt(0))
  v.setUint8(38, 't'.charCodeAt(0))
  v.setUint8(39, 'a'.charCodeAt(0))

  v.setInt32(40, buf.length * 2 * bytes_per_sample) // length of data

  left = buf.getChannelData(0)
  right = buf.getChannelData(1)
  console.log(left)

  return out
}
